<?php

use App\Http\Controllers\TodoController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::controller(TodoController::class)->group(function() {
    Route::get('/todo', 'getTodo');
    Route::post('/todo', 'saveTodo');
    Route::put('/todo/{iTodoId}', 'updateTodo');
    Route::delete('/todo/{iTodoId}', 'deleteTodo');
    Route::put('/reorder', 'reorderTodo');
});
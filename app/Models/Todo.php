<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @class Todo
 * @package App\Models
 * @author Francis Derit
 * @since 2023.07.21
 */
class Todo extends Model
{
    /**
     * Table name
     */
    protected $table = 't_todo';

    /**
     * Primary key
     */
    protected $primaryKey = 'todo_id';

    /**
     * Fillable columns
     */
    protected $fillable = [
        'todo_description'
    ];
}

<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @class TodoController
 * @package App\Http\Controllers
 * @author Francis Derit
 * @since 2023.07.21
 */
class TodoController extends Controller
{
    /**
     * Request for fetching todo list
     *
     * @return JsonResponse
     */
    public function getTodo(): JsonResponse
    {
        $aTodo = Todo::orderBy('todo_position', 'asc')
            ->get()
            ->toArray();

        return response()->json([
            'success' => true,
            'todo_list' => $aTodo
        ]);
    }

    /**
     * Request for creating a todo
     *
     * @param Request $oRequest
     * @return JsonResponse
     */
    public function saveTodo(Request $oRequest): JsonResponse
    {
        try {
            // This will validate the parameters
            $oRequest->validate([
                'todo_description' => 'required|string',
                'todo_position' => 'required|int'
            ]);

            $aParams = $oRequest->all();

            // This will create new todo in the table
            $iTodoId = Todo::insertGetId($aParams);

            return response()->json([
                'success' => true,
                'todo_id' => $iTodoId
            ]);
        } catch (\Exception $oException) {
            return response()->json([
                'success' => false
            ]);
        }
    }

    /**
     * Request for updating a todo
     *
     * @param Request $oRequest
     * @param int $iTodoId
     * @return JsonResponse
     */
    public function updateTodo(Request $oRequest, int $iTodoId): JsonResponse
    {
        try {
            // This will validate the parameters
            $oRequest->validate([
                'todo_description' => 'required|string',
            ]);

            $aParams = $oRequest->all();

            // This will update the todo by id
            Todo::where('todo_id', $iTodoId)
                ->update($aParams);

            return response()->json([
                'success' => true,
                'todo_id' => $iTodoId
            ]);
        } catch (\Exception $oException) {
            return response()->json([
                'success' => false,
            ]);
        }
    }

    /**
     * Request for deleting a todo
     *
     * @param Request $oRequest
     * @param int $iTodoId
     * @return JsonResponse
     */
    public function deleteTodo(Request $oRequest, int $iTodoId): JsonResponse
    {
        try {
            // This will delete the todo by id
            Todo::where('todo_id', $iTodoId)->delete();

            $this->updateTodoPosition();

            return response()->json([
                'success' => true,
                'todo_id' => $iTodoId
            ]);
        } catch (\Exception $oException) {
            return response()->json([
                'success' => false,
            ]);
        }
    }


    /**
     * This will update the position of todos
     *
     * @return void
     */
    private function updateTodoPosition(): void
    {
        // This will reset the todo position to 0
        Todo::query()->update(['todo_position' => null]);

        // This will fetch the todo in order by ascending
        $aTodos = Todo::orderBy('todo_position', 'asc')
            ->get()
            ->toArray();

        // This will update the todo new position
        foreach ($aTodos as $iPosition => $aTodo) {
            $iTodoId = $aTodo['todo_id'];

            Todo::where('todo_id', $iTodoId)
                ->update(['todo_position' => $iPosition + 1]);
        }
    }

    /**
     * Request for reordering todos
     *
     * @param Request $oRequest
     * @return JsonResponse
     */
    public function reorderTodo(Request $oRequest): JsonResponse
    {
        try {
            // This will validate the parameters
            $oRequest->validate([
                'todos' => 'required|array',
                'todos.*.todo_id' => 'required|distinct|int',
                'todos.*.todo_position' => 'required|int|distinct',
            ]);

            $aParams = $oRequest->all();

            $aTodosNewPosition = Arr::get($aParams, 'todos');
            $aTodosDbCount = count(Todo::all()->toArray());

            // This will check if parameter count is the same with the database value
            if (count($aTodosNewPosition) !== $aTodosDbCount) {
                return response()->json([
                    'success' => false
                ]);
            }

            // This will reset the todo position to 0
            Todo::query()->update(['todo_position' => null]);
//            if (count($aTodosOrder) !== )
            foreach ($aTodosNewPosition as $aTodo) {
                $iTodoId = $aTodo['todo_id'];
                $iTodoPosition = $aTodo['todo_position'];

                Todo::where('todo_id', $iTodoId)
                    ->update(['todo_position' => $iTodoPosition]);
            }

            return response()->json([
                'success' => true
            ]);
        } catch (\Exception $oException) {
            return response()->json([
                'success' => false
            ]);
        }
    }
}

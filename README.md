## Setup Configuration
- Execute: **composer install**
- Start your MySQL server
- Create a database named **todo_list**
- Migrate the database by executing: **php artisan migrate:fresh**
- Start the project development server by executing: **php artisan serve**
- Access the API endpoints
